# grape_swagger-uiサンプルアプリケーション

## 前提

下記を使用できるようにしておいてください。

* Ruby 2.3.0以上
* bundler

## 使用方法

下記コマンドを実行してください。

    $ git clone https://bitbucket.org/lookme/grape_swagger-ui_sample.git
    $ cd grape_swagger-ui_sample
    $ bundle install
    $ bin/rails db:migrate
    $ bin/rails db:seed
    $ bin/rails s

コンソールに `* Listening on tcp://localhost:3000` と表示がされたら、
`http://localhost:3000/swagger` にアクセスするとswagger-uiの画面にアクセスできます。

各APIの「Try it out!」ボタンを押すと実際にAPIにリクエストを送ることができますので、是非試して見てください。
