# frozen_string_literal: true

class Users < Grape::API
  default_format :json

  helpers do
    def user_params
      ActionController::Parameters.new(declared(params, include_missing: false))
        .permit(:name, :age, { emails_attributes: [:id, :mail_address, :_destroy] })
    end
  end

  resources :users do
    desc 'Index users' do
      headers({ :'X-Custom-Header' => { description: 'Your custom header', required: false } })
      success UserEntity
      failure [
                [500, 'Server error', ErrorEntity],
              ]
    end
    get do
      users = User.all
      present users, with: UserEntity, except: [:emails]
    end

    desc 'Create a user' do
      headers({ :'X-Custom-Header' => { description: 'Your custom header', required: false } })
      success UserEntity
      failure [
                [400, 'Validation error', ErrorEntity],
                [500, 'Server error', ErrorEntity],
              ]
    end
    params do
      requires :name, type: String, documentation: { param_type: 'body' }
      requires :age, type: Integer, documentation: { param_type: 'body' }
      optional :emails_attributes, type: Array, documentation: { param_type: 'body' } do
        requires :mail_address, type: String, documentation: { param_type: 'body' }
      end
    end
    post do
      user = User.new(user_params)
      user.save!
      present user, with: UserEntity
    end

    route_param :id do
      desc 'Show a user' do
        headers({ :'X-Custom-Header' => { description: 'Your custom header', required: false } })
        success UserEntity
        failure [
                  [400, 'Validation error', ErrorEntity],
                  [404, 'Not found error', ErrorEntity],
                  [500, 'Server error', ErrorEntity],
                ]
      end
      params do
        requires :id, type: Integer, documentation: { param_type: 'path' }
      end
      get do
        user = User.includes(:emails).find(params[:id])
        present user, with: UserEntity
      end

      desc 'Update a user' do
        headers({ :'X-Custom-Header' => { description: 'Your custom header', required: false } })
        success UserEntity
        failure [
                  [404, 'Not found error', ErrorEntity],
                  [500, 'Server error', ErrorEntity],
                ]
      end
      params do
        requires :id, type: Integer, documentation: { param_type: 'path' }
        optional :name, type: String, documentation: { param_type: 'body' }
        optional :age, type: Integer, documentation: { param_type: 'body' }
        optional :emails_attributes, type: Array, documentation: { param_type: 'body' } do
          optional :id, type: Integer, documentation: { param_type: 'body' }
          optional :mail_address, type: String, documentation: { param_type: 'body' }
          optional :_destroy, type: Boolean, documentation: { param_type: 'body' }
        end
      end
      patch do
        user = User.includes(:emails).find(params[:id])
        user.assign_attributes(user_params)
        user.save!
        present user, with: UserEntity
      end

      desc 'Delete a user' do
        headers({ :'X-Custom-Header' => { description: 'Your custom header', required: false } })
        failure [
                  [404, 'Not found error', ErrorEntity],
                  [500, 'Server error', ErrorEntity],
                ]
      end
      params do
        requires :id, type: Integer, documentation: { param_type: 'path' }
      end
      delete do
        user = User.find(params[:id])
        user.destroy!
      end
    end
  end

  rescue_from :all do |e|
    status = case e
             when ActiveRecord::RecordInvalid
               400
             when ActiveRecord::RecordNotFound
               404
             else
               500
             end
    error!({ message: e.message }, status)
  end

  route :any, '*path' do
    error!({ message: 'No such api' }, 404)
  end

  unless Rails.env.production?
    add_swagger_documentation(
      base_path: '/api',
      hide_documentation_path: true,
      format: :json,
      info: {
        title: 'grape_swagger-uiサンプルアプリケーション',
      },
    )
  end
end
