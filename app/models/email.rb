# frozen_string_literal: true

class Email < ApplicationRecord
  belongs_to :user, optional: true
end
