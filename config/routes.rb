Rails.application.routes.draw do
  mount Users => '/api'
  if defined? GrapeSwaggerRails
    mount GrapeSwaggerRails::Engine => '/swagger'
  end
end
