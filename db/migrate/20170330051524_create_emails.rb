class CreateEmails < ActiveRecord::Migration[5.0]
  def change
    create_table :emails do |t|
      t.belongs_to :user, foreign_key: true
      t.string :mail_address

      t.timestamps
    end
  end
end
